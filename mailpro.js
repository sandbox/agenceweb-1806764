function remove_class(){
	for(i=1; i<26;i++)
	{
		var count = i;
		$('#item-mailpro-field'+count+'-wrapper').text('').removeClass('error');
		$('#edit-field'+count).removeClass('error').val('')
	}
}

function add_emails(idclient, apikey, address_book_id){
	var process = true;
	if(idclient == '') 
	{
		alert('Please check configration settings. Client is missing.');
		return false;
	}
	
	if(apikey == '') 
	{
		alert('Please check configration settings. APIKey is missing.');
		return false;
	}
	
	if(address_book_id == '') 
	{
		alert('Please check configration settings. AddressBookID is missing.');
		return false;
	}
	
	$('#item-mailpro-wrapper').text('').removeClass('error').removeClass('status');
	
	
	var emailList = $('#edit-email').val();
	
	if(emailList == '')
	{
		$('#edit-email').addClass('error');
		$('#item-mailpro-email-wrapper').text('Please Enter Email first.').addClass('error');
		process = false;
	}
	
	if(process && emailList)
	{
		var hs_email = emailList;
		
		var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	
		var datastring = '';
		if (filter.test(hs_email))
		{
			datastring = datastring+hs_email
			for(i=1; i<26;i++)
			{
				
				var count = i;
				if($('#edit-field'+count).attr('id')){
					if($('#edit-field'+count).val() == ''){
						$('#edit-field'+count).addClass('error');
						$('#item-mailpro-field'+count+'-wrapper').text('This field is required.').addClass('error');
						process = false;
					}else if($('#edit-field'+count).val()){
						$('#item-mailpro-field'+count+'-wrapper').text('').removeClass('error');
						datastring = datastring+','+$('#edit-field'+count).removeClass('error').val().split(',')[0];
					}
				}else{
					datastring = datastring+',';
				}
			}
			
			if(process == false){
				$('#edit-email').removeClass('error');
				$('#item-mailpro-email-wrapper').text('').removeClass('error');
				return false;	
			}
			
			$('#item-mailpro-wrapper').text('Your Request Sending.....');
			
			var xmlhttp;
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
			  	xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
			  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			  
			xmlhttp.onreadystatechange=function()
		  	{
		  		if (xmlhttp.readyState==4)
				{
					$('#item-mailpro-wrapper').text('Request sent successfully. ').addClass('status');
					$('#edit-email').val('');
					remove_class();
				}
		  	}
			xmlhttp.open("POST","https://api.mailpro.com/v2/email/add.xml",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("IDClient="+idclient+"&APIKey="+apikey+"&AddressBookID="+address_book_id+"&force+2&emailList="+datastring);
			
		}
		else
		{
			$('#edit-email').addClass('error');
			$('#item-mailpro-email-wrapper').text('Please enter valid email.').addClass('error');
		}
	}
	
	return false;	
}