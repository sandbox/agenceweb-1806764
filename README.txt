-- SUMMARY --

The Drupal mailpro module displays the block form to submit users emails and 
other data to mailpro account addressbook.

For a full description of the module, visit the project page:
  http://drupal.org/project/mailpro

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/mailpro


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  mailpro module:

  - mailpro content

    Users in roles with the "mailpro content" permission will see
    the mailpro block form.

* Customize the menu settings in Administer >> Site configuration >>
  MailPro menu.

* For configure settings first of all please enter your
	your mailpro Client Id and mailpro APIKey and click save configration button.
	
* For next after above step you will see text field for AddressBook.
	please select an AddressBook for submiting users email and data.
	
* For Show block form go to block and select Region for MailPro form
	where form will be display.
	
-- TROUBLESHOOTING --

* If the MailPro form does not display, check the following:

  - Are the "mailpro content"
    permissions enabled for the appropriate roles?


-- CONTACT --

Current maintainers:
* HermesIPServices (sun) - http://drupal.org/user/231041

